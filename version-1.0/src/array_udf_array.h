/**
 *ArrayUDF Copyright (c) 2017, The Regents of the University of California, through Lawrence Berkeley National Laboratory (subject to receipt of any required approvals from the U.S. Dept. of Energy).  All rights reserved.
 *
 *If you have questions about your rights to use or distribute this software, please contact Berkeley Lab's Innovation & Partnerships Office at  IPO@lbl.gov.
 *
 * NOTICE. This Software was developed under funding from the U.S. Department of Energy and the U.S. Government consequently retains certain rights. As such, the U.S. Government has been granted for itself and others acting on its behalf a paid-up, nonexclusive, irrevocable, worldwide license in the Software to reproduce, distribute copies to the public, prepare derivative works, and perform publicly and display publicly, and to permit other to do so. 
 *
 */

/**
 *
 * Email questions to {dbin, kwu, sbyna}@lbl.gov
 * Scientific Data Management Research Group
 * Lawrence Berkeley National Laboratory
 *
 */

#ifndef ARRAY_UDF_ARRAY
#define ARRAY_UDF_ARRAY


#include <assert.h>
#include "mpi.h"
#include "array_udf_io.h"
#include "array_udf_stencil.h"
#include "utility.h"

int                 save_result_flag      = 1;
int                 trail_run_flag        = 0;
int                 row_major_chunk_flag  = 0;
unsigned long long  per_core_mem_size_limit = 240000000; // Try a smller size 24000000000; //50GB/24 per core Byte


//Comment out following test codes
//int      arraystoreloadflat    = 0;
//int      handop_flag           = 0;
//int      single_step_flag      = 0;
//#define  HANDOP3D   1


using namespace std;

template <class T>
class Array{
private:
  int                             mpi_rank;  //MPI information
  int                             mpi_size; //MPI information

  Data<T>                  *data_on_disk;      //the data on disk to apply UDF (user provide)
  std::vector<int>                data_chunk_size;   //size of each chunk (user provide)
  std::vector<int>                data_overlap_size; //size of overlapping  (user provide)
  int                             data_dims;              //The dimensioins of data_on_disk , equal to dsize.size()
  std::vector<unsigned long long> data_dims_size;         //The size of each dimension (global, extracted from data_on_disk)
  unsigned long long              data_total_chunks;      //The total number of chunks (global)
  std::vector<unsigned long long> data_chunked_dims_size;       //The number of chunks per dimenstion
     
  
  int                             current_chunk_id;           //Id of the current chunk (in memory) to apply UDF
  std::vector<T>                  current_chunk_data;         //Pointer to data of current chunk
 
  std::vector<unsigned long long> current_chunk_start_offset; //Start offset on disk
  std::vector<unsigned long long> current_chunk_end_offset;   //End offset on disk
  std::vector<unsigned long long> current_chunk_size;         //Size of the chunk, euqal to end_offset - start_offset
  unsigned long long              current_chunk_cells;         //The number of cells in current chunk

  std::vector<unsigned long long> current_chunk_ol_start_offset; //Start offset on disk with overlapping
  std::vector<unsigned long long> current_chunk_ol_end_offset;   //End offset on disk with overlapping
  std::vector<unsigned long long> current_chunk_ol_size;         //Size of the chunk, euqal to end_offset - start_offset
  unsigned long long              current_chunk_ol_cells;         //The number of cells in current chunk
  std::vector<         long long> ol_origin_offset;         //Size of the chunk, euqal to end_offset - start_offset
  std::vector<T>                  current_result_chunk_data;         //Pointer to data of current chunk
  double time_read, time_udf, time_write, t_start, t_end;
public:
  //Do Nothing
  Array(){};

  //To store the result. The result file is delayed to be created
  Array(std::string fn, std::string gn, std::string dn){
    MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);
    data_on_disk   =  new Data<T>(fn, gn, dn, DELAY_TO_CREATE);
  };

  //Input file:
  //HDF Specific Intialization
  //   fn: file name
  //   gn: group name
  //   dn: data set name
  //cs and os: user defined chunk size and overlap size
  Array(std::string fn, std::string gn, std::string dn, std::vector<int> cs, std::vector<int> os){
    MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);
    data_on_disk =  new Data<T>(fn, gn, dn);
    data_chunk_size   = cs;
    data_overlap_size = os;

    data_dims_size = data_on_disk->GetDimSize();
    data_dims      = data_dims_size.size();

    if (row_major_chunk_flag == 1){
      if(mpi_rank == 0 )
        std::cout << "Auto row-major partition ... " << std::endl;
      unsigned long long node_size_limit = 1;
      for (int i = 0; i < data_dims; i++){
        node_size_limit = data_dims_size[i]*node_size_limit;
      }
      node_size_limit = node_size_limit / mpi_size;

      if (node_size_limit > (per_core_mem_size_limit/sizeof(T))){
        node_size_limit  = per_core_mem_size_limit/sizeof(T);
        if(mpi_rank == 0){
          std::cout << "Using the memory size as limit !" << std::endl;
	}
      }
    
      std::vector<unsigned long long> chunk_size_temp= RowMajorOrderReverse(node_size_limit, data_dims_size);
 
      if(mpi_rank == 0 ){
        std::cout << " Node_size_limit =  " << node_size_limit << std::endl;
        for (int i = 0; i < data_dims ; i++){
          std::cout << chunk_size_temp[i] << std::endl;
        }
      }


      int replace_flag = 1;
      for (int i = data_dims-1; i > 0 ; i--){
        if (chunk_size_temp[i] != data_dims_size[i]){
          chunk_size_temp[i] = data_dims_size[i];
          if(chunk_size_temp[i-1] != 0){
            chunk_size_temp[i-1] = chunk_size_temp[i-1]-1;
          }else{
            replace_flag = 0;
            break;
          }
        }
      }

      for (int i = data_dims-1; i >= 0 ; i--){
        if (chunk_size_temp[i] == 0)
          replace_flag = 0;
      }

      if(replace_flag){
        if (mpi_rank == 0)
          std::cout << " New chunk size: ";
        for (int i = data_dims-1; i >= 0 ; i--){
          data_chunk_size[i] = chunk_size_temp[i];
          if (mpi_rank == 0)
            std::cout << data_chunk_size[i] << " , " ;
        }
        if (mpi_rank == 0)
          std::cout << std::endl;
      }else{
    	if (mpi_rank == 0)
	  std::cout << " No auto-chunk ! " << std::endl;
      }
    }

    current_chunk_start_offset.resize(data_dims);
    current_chunk_end_offset.resize(data_dims);
    current_chunk_size.resize(data_dims);

    current_chunk_ol_start_offset.resize(data_dims);
    current_chunk_ol_end_offset.resize(data_dims);
    current_chunk_ol_size.resize(data_dims);
       
    data_chunked_dims_size.resize(data_dims);
    ol_origin_offset.resize(data_dims);
       
    data_total_chunks = 1;
    if(mpi_rank == 0)
      std::cout << "data rank = " << data_dims << std::endl;
    for(int i = 0; i < data_dims; i++){
      if(data_dims_size[i]%data_chunk_size[i] == 0){
        data_chunked_dims_size[i] = data_dims_size[i]/data_chunk_size[i];  
      }else{
        data_chunked_dims_size[i] = data_dims_size[i]/data_chunk_size[i] + 1; 
      }
      data_total_chunks = data_total_chunks * data_chunked_dims_size[i];
    }
       
    if(mpi_rank == 0){
      std::cout << "File    : " << fn  <<std::endl;	    
      std::cout << "Group   : " << gn  <<std::endl;	    	
      std::cout << "Dataset : " << dn  <<std::endl;	    		

      std::cout << "data size  = " ;
      for(int i = 0; i < data_dims; i++){
        std::cout << ", " << data_dims_size[i];
      }
      std::cout << std::endl;

      std::cout << "chunk size  = " ;
      for(int i = 0; i < data_dims; i++){
        std::cout << ", " << data_chunk_size[i];
      }
      std::cout << std::endl;

      std::cout << "overlap size = " ;
      for(int i = 0; i < data_dims; i++){
        std::cout << ", " << os[i];
      }
      std::cout << std::endl;
    }

    if(data_total_chunks%mpi_size != 0){
      data_on_disk->DisableCollectivIO();
    }
       

    current_chunk_id = mpi_rank;  //Each process deal with one chunk one time, starting from its rank
  }; 

     
  ~Array(){
    delete data_on_disk;
  }
    
  //Return  1, data read into   local_chunk_data
  //Return  0, end of file (no data left to handle)
  //Return -1: error happen
  int LoadNextChunk(){
    //std::cout <<  "Load chunk id =" << current_chunk_id << std::endl;
    if(current_chunk_id >= data_total_chunks){
      return 0;
    }
       
    current_chunk_cells = 1;
    current_chunk_ol_cells = 1;
    std::vector<unsigned long long> chunk_coordinate = RowMajorOrderReverse(current_chunk_id, data_chunked_dims_size);
    for(int i = 0 ; i < data_dims; i++){
      if(data_chunk_size[i] * chunk_coordinate[i] < data_dims_size[i]){
        current_chunk_start_offset[i] = data_chunk_size[i] * chunk_coordinate[i]; 
      }else{
        current_chunk_start_offset[i] = data_dims_size[i]; 
      }
         
      if(current_chunk_start_offset[i] + data_chunk_size[i] -1 < data_dims_size[i]){
        current_chunk_end_offset[i] = current_chunk_start_offset[i]+data_chunk_size[i] - 1; 
      }else{
        current_chunk_end_offset[i] = data_dims_size[i] - 1; 
      }
      assert((current_chunk_end_offset[i] - current_chunk_start_offset[i] + 1 >= 0));
      current_chunk_size[i] = current_chunk_end_offset[i] - current_chunk_start_offset[i] + 1;
      current_chunk_cells  =  current_chunk_cells * current_chunk_size[i];

      //Deal with overlapping
      //Starting coordinate for the data chunk with overlapping
      if(current_chunk_start_offset[i] <= data_overlap_size[i]){
        current_chunk_ol_start_offset[i] = 0;
      }else{
        current_chunk_ol_start_offset[i] =  current_chunk_start_offset[i] - data_overlap_size[i];
      }
      //Original coordinate offset 
      ol_origin_offset[i] = current_chunk_start_offset[i] - current_chunk_ol_start_offset[i];

      //Ending oordinate for the data chunk with overlapping
      if(current_chunk_end_offset[i] + data_overlap_size[i] < data_dims_size[i]){
        current_chunk_ol_end_offset[i] =  current_chunk_end_offset[i] + data_overlap_size[i];
      }else{
        current_chunk_ol_end_offset[i] = data_dims_size[i] - 1;
      }

      current_chunk_ol_size[i]   =   current_chunk_ol_end_offset[i] - current_chunk_ol_start_offset[i] + 1;
      current_chunk_ol_cells     =   current_chunk_ol_cells * current_chunk_ol_size[i];
    }


    if(mpi_rank == 0)
      std::cout <<  "Load chunk id =" << current_chunk_id  << " ...  done , at proc  "  << mpi_rank << std::endl;
#ifdef DEBUG
    if(mpi_rank == 0){
      std::cout <<  "Load chunk id =" << current_chunk_id  << ", at proc "  << mpi_rank << std::endl;
      std::cout <<  "chunk_coordinate[] =" << chunk_coordinate[0] << "," << chunk_coordinate[1] <<  std::endl;
      std::cout <<  "current_chunk_start_offset [] =" << current_chunk_start_offset[0] << ","<< current_chunk_start_offset[1] << std::endl;
      std::cout <<  "current_chunk_end_offset [] ="   << current_chunk_end_offset[0] << "," << current_chunk_end_offset[1]<< std::endl;
      std::cout <<  "current_chunk_size [] ="   << current_chunk_size[0] << "," << current_chunk_size[1]<< std::endl;
      std::cout <<  "data_total_chunks = " <<  data_total_chunks << std::endl;
       
      std::cout <<  "current_chunk_ol_start_offset [] =" << current_chunk_ol_start_offset[0] << ","<< current_chunk_ol_start_offset[1] << std::endl;
      std::cout <<  "current_chunk_ol_end_offset [] ="   << current_chunk_ol_end_offset[0] << "," << current_chunk_ol_end_offset[1]<< std::endl;
      std::cout <<  "current_chunk_ol_size [] = " << current_chunk_ol_size[0] << ", " << current_chunk_ol_size[1] << std::endl; 
      std::cout <<  "ol_origin_offset [] = " << ol_origin_offset[0] << ", " << ol_origin_offset[1] << " \n"<< std::endl; 
    }
#endif

    //Next chunk id
    current_chunk_id = current_chunk_id + mpi_size;
       
       
    //Return  1, data read into   local_chunk_data
    //Return  0, end of file (no data left to handle)
    //Return -1: error happen
    //Read data between local_chunk_start_offset and local_chunk_end_offset
    //current_chunk_data.resize(current_chunk_cells);
    current_chunk_data.resize(current_chunk_ol_cells);
    current_result_chunk_data.resize(current_chunk_cells);
    //return data_on_disk->ReadData(current_chunk_start_offset, current_chunk_end_offset, current_chunk_data);
    return data_on_disk->ReadData(current_chunk_ol_start_offset, current_chunk_ol_end_offset, current_chunk_data);
  }
  //The interface to accept UDF function.
  //If user want to use python interface, the UDF has "void *" parameter
  //In native c++, the UDF accepts "const Stencil<T> &"
  //Main reason: for performance and usability. 
#ifdef PY_ARRAYUDF
  void Apply(T (*UDF)(void *), Array<T> *B){
#else 
  void Apply(T (*UDF)(const Stencil<T> &), Array<T> *B){
#endif
    current_chunk_id = mpi_rank;  //Reset to starting point

      //Give the trail run to go and get the maximum overlap size
      if(mpi_rank == 0){
	if( trail_run_flag == 1){
          std::cout << "Trailrun start  ... " << std::endl;
    	}else{
          std::cout << "Trailrun is disabled !  " << std::endl;
    	}
      }
      if (trail_run_flag == 1){ 
	t_start =  MPI_Wtime();
    	std::vector<T>    trail_data(1, 1.0);
    	Stencil<T> trail_cell(data_dims, &trail_data[0]);
#ifdef  PY_ARRAYUDF
        UDF(&trail_cell); 
#else
    	UDF(trail_cell);
#endif
    	trail_cell.get_trail_run_result(&data_overlap_size[0]);
    	t_end =  MPI_Wtime();
    	trail_run_time = trail_run_time + t_end - t_start;
    
    	if(mpi_rank == 0){
          std::cout << "Trailrun results: overlap size = : ";
          for(int i = 0; i < data_dims; i++){
            std::cout << ", " << data_overlap_size[i];
          }
          std::cout << std::endl;
    	}
      }
    
      //Start real UDF function running
      B->CreateDiskFile(data_dims, data_dims_size, data_chunk_size, data_overlap_size,  data_on_disk->GetTypeClass(), data_total_chunks);
      if(data_total_chunks%mpi_size != 0){
        B->DisableCollectivIO(B->GetFileName());
      }
       
      MPI_Barrier(MPI_COMM_WORLD);

      std::vector<unsigned long long> cell_coordinate, cell_coordinate_ol;
      unsigned long long offset_ol;
      cell_coordinate_ol.resize(data_dims);
      for(int iii = 0; iii < data_dims; iii++){
        cell_coordinate_ol[iii] = 0;
      }
    
      time_read = 0;
      time_write = 0;
      time_udf = 0;
    
      t_start =  MPI_Wtime();
      int load_ret = LoadNextChunk();
      t_end =  MPI_Wtime();
      time_read = time_read + t_end - t_start;
    
      if(mpi_rank == 0)
        std::cout <<  "Loading the first chunk ... done ! "  << std::endl;
    
      while(load_ret == 1){
        T cell_return_value;
        Stencil<T> cell_target(0,  &current_chunk_data[0], cell_coordinate_ol, current_chunk_ol_size);
      
        //Apply UDF to eah cell of local chunk, untill no chunk
        for(unsigned long long i=0; i < current_chunk_cells; i++){
#ifdef DEBUG
          std::cout << "current_chunk_data[i] =" << current_chunk_data[i] << std::endl;
#endif
          //Get the coordinate (HDF5 uses row major layout)
          cell_coordinate = RowMajorOrderReverse(i, current_chunk_size);
        
          //Get the coodinate with overlapping
          for (int ii=0; ii < data_dims; ii++){
            if (cell_coordinate[ii] + ol_origin_offset[ii] < current_chunk_ol_size[ii]){
              cell_coordinate_ol[ii] = cell_coordinate[ii] + ol_origin_offset[ii];
            }else{
              cell_coordinate_ol[ii] = current_chunk_ol_size[ii]-1;
            }
          }
         
          //Update the offset with overlapping
          offset_ol = RowMajorOrder(current_chunk_ol_size, cell_coordinate_ol);
          cell_target.SetLocation(offset_ol, cell_coordinate_ol);

          t_start =  MPI_Wtime();
#ifdef  PY_ARRAYUDF
          cell_return_value = UDF(&cell_target); // Called by python
#else
        cell_return_value = UDF(cell_target); // Called by C++ 
#endif
          t_end   =  MPI_Wtime();

          time_udf = t_end-t_start + time_udf;

#ifdef DEBUG  
          if (current_chunk_id == 1){
            std::cout << "cell_coordinate    = " << cell_coordinate[0]    << ", " << cell_coordinate[1]  << std::endl;
            std::cout << "ol_origin_offsest  = " << ol_origin_offset[0]  << ", "  << ol_origin_offset[1] << std::endl;
            std::cout << "cell_coordinate_ol = " << cell_coordinate_ol[0] << ", " << cell_coordinate_ol[1] << std::endl;
            std::cout << "offset_ol = " << offset_ol << std::endl;
            std::cout << std::endl;
          }
#endif
          current_result_chunk_data[i] =   cell_return_value;       //cell_return =  cell_return.get_value();
#ifdef DEBUG
          std::cout << "current_result_chunk_data[" <<i << "] = "  << current_result_chunk_data[i] << std::endl;
#endif
        }

        if(mpi_rank == 0)
          std::cout <<  "Process data eletemes ... done !" << std::endl;
        //if(single_step_flag == 1)
        // goto report_results_mark;
    
        t_start =  MPI_Wtime(); 
        if(save_result_flag)
          B->SaveResult(current_chunk_start_offset, current_chunk_end_offset, current_result_chunk_data);
        t_end =  MPI_Wtime(); 
        time_write = time_write + t_end - t_start;
	 
        t_start =  MPI_Wtime();  
        load_ret = LoadNextChunk();
        t_end =  MPI_Wtime(); 
        time_read = time_read + t_end - t_start;
      }
      current_chunk_id = mpi_rank;  //Reset to starting point
   report_results_mark:
      ReportTime();
    }
     
    int  GetRank(){
      return data_dims;
    }
     
    std::vector<unsigned long long> GetDims(){
      return data_dims_size;
    }

    void ReportTime(){
      double time_read_max, time_write_max, time_udf_max, time_address_cal_max, row_major_order_cal_max, pre_row_major_order_cal_max, data_access_time_max;     	
      double time_read_min, time_write_min, time_udf_min, time_address_cal_min, row_major_order_cal_min, pre_row_major_order_cal_min, data_access_time_min;     	
      double time_read_sum, time_write_sum, time_udf_sum, time_address_cal_sum, row_major_order_cal_sum, pre_row_major_order_cal_sum, data_access_time_sum;     	
      double trail_run_time_max, trail_run_time_min, trail_run_time_sum;



      MPI_Allreduce(&time_read,                 &time_read_max,                 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
      MPI_Allreduce(&time_write,                &time_write_max,                1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
      MPI_Allreduce(&time_udf,                  &time_udf_max,                  1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);


      MPI_Allreduce(&time_read,                 &time_read_min,                 1, MPI_DOUBLE, MPI_MIN, MPI_COMM_WORLD);
      MPI_Allreduce(&time_write,                &time_write_min,                1, MPI_DOUBLE, MPI_MIN, MPI_COMM_WORLD);
      MPI_Allreduce(&time_udf,                  &time_udf_min,                  1, MPI_DOUBLE, MPI_MIN, MPI_COMM_WORLD);
    

      MPI_Allreduce(&time_read,  &time_read_sum,  1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
      MPI_Allreduce(&time_write, &time_write_sum, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
      MPI_Allreduce(&time_udf,   &time_udf_sum,   1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

      MPI_Allreduce(&time_address_cal,          &time_address_cal_max,          1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
      MPI_Allreduce(&time_address_cal,          &time_address_cal_min,          1, MPI_DOUBLE, MPI_MIN, MPI_COMM_WORLD);
      MPI_Allreduce(&time_address_cal,          &time_address_cal_sum,          1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

      MPI_Allreduce(&row_major_order_cal,       &row_major_order_cal_max,       1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
      MPI_Allreduce(&row_major_order_cal,       &row_major_order_cal_min,       1, MPI_DOUBLE, MPI_MIN, MPI_COMM_WORLD);
      MPI_Allreduce(&row_major_order_cal,       &row_major_order_cal_sum,       1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

      MPI_Allreduce(&pre_row_major_order_cal,   &pre_row_major_order_cal_max,   1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
      MPI_Allreduce(&pre_row_major_order_cal,   &pre_row_major_order_cal_min,   1, MPI_DOUBLE, MPI_MIN, MPI_COMM_WORLD);
      MPI_Allreduce(&pre_row_major_order_cal,   &pre_row_major_order_cal_sum,   1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);


      MPI_Allreduce(&data_access_time,   &data_access_time_max,   1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
      MPI_Allreduce(&data_access_time,   &data_access_time_min,   1, MPI_DOUBLE, MPI_MIN, MPI_COMM_WORLD);
      MPI_Allreduce(&data_access_time,   &data_access_time_sum,   1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

      MPI_Allreduce(&trail_run_time,                  &trail_run_time_max,                  1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
      MPI_Allreduce(&trail_run_time,                  &trail_run_time_min,                  1, MPI_DOUBLE, MPI_MIN, MPI_COMM_WORLD);
      MPI_Allreduce(&trail_run_time,                  &trail_run_time_sum,                  1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

      if(mpi_rank == 0){
        std::cout << "Results on Rank 0" << std::endl;
        std::cout << "Trailrun  time (s)  :  = " << trail_run_time  << std::endl;
        std::cout << "Read      time (s)  :  = " << time_read  << std::endl;
        std::cout << "UDF       time (s)  :  = " << time_udf << std::endl;
        std::cout << "->AdrCal  time (s)  :  = " << time_address_cal << std::endl;
        std::cout << "------->Pre-RMO  time (s) :  = " << pre_row_major_order_cal << std::endl;
        std::cout << "------->    RMO  time (s) :  = " << row_major_order_cal << std::endl;
        std::cout << "->DA      time (s)  :  = " << data_access_time << std::endl;
        std::cout << "Write     time (s)  :  = " << time_write << std::endl;
      }	

      if(mpi_rank == 0){
        std::cout << "Results of All " << std::endl;
        std::cout << "Trailrun  time (s) : max = " << trail_run_time_max << ", min = " << trail_run_time_min << ", ave = " << trail_run_time_sum/mpi_size << std::endl;
        std::cout << "Read      time (s) : max = " << time_read_max << ", min = " << time_read_min << ", ave = " << time_read_sum/mpi_size << std::endl;
        std::cout << "UDF       time (s) : max = " << time_udf_max << ", min = " << time_udf_min << ", ave = " << time_udf_sum/mpi_size << std::endl;
        std::cout << "->AdrCal  time (s) : max = "  << time_address_cal_max        << ", min = "  << time_address_cal_min        << ", ave = " << time_address_cal_sum/mpi_size << std::endl;
        std::cout << "------->Pre-RMO time (s) : max = " << pre_row_major_order_cal_max << ", min = "  << pre_row_major_order_cal_min << ", ave = " << pre_row_major_order_cal_sum/mpi_size << std::endl;
        std::cout << "------->    RMO time (s) : max = " << row_major_order_cal_max     << ", min = "  << row_major_order_cal_min     << ", ave = " << row_major_order_cal_sum/mpi_size << std::endl;
        std::cout << "->DA      time (s) : max = " << data_access_time_max     << ", min = "  << data_access_time_min     << ", ave = " << data_access_time_sum/mpi_size << std::endl;
        std::cout << "Write     time (s) : max = " << time_write_max << ", min = " << time_write_min << ", ave = " << time_write_sum/mpi_size << std::endl;
      }	

    }

    int CreateDiskFile(int data_dims, std::vector<unsigned long long> data_dims_size,   std::vector<int> data_chunk_size,
                       std::vector<int> data_overlap_size, int type_class, unsigned long long  data_total_chunks_p){
      data_dims =  data_dims;
      data_dims_size = data_dims_size;
      data_chunk_size = data_chunk_size;
      data_overlap_size = data_overlap_size;
      data_total_chunks =  data_total_chunks_p;
      return data_on_disk->CreateDiskFile(data_dims, data_dims_size, type_class);
    }

    void DisableCollectivIO(std::string fn){
      data_on_disk->DisableCollectivIO();
      if(mpi_rank == 0){
        std::cout << "Disable Collective IO for " << fn << std::endl;
      }
    }
  
    int SaveResult(std::vector<unsigned long long> current_chunk_start_offset, std::vector<unsigned long long> current_chunk_end_offset, std::vector<T> current_result_chunk_data){
      return data_on_disk->WriteData(current_chunk_start_offset, current_chunk_end_offset, current_result_chunk_data);
    }

    std::string GetFileName(){
      return data_on_disk->GetFileName();
    }

     
  };

#endif
