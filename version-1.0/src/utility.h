/**
 *ArrayUDF Copyright (c) 2017, The Regents of the University of California, through Lawrence Berkeley National Laboratory (subject to receipt of any required approvals from the U.S. Dept. of Energy).  All rights reserved.
 *
 *If you have questions about your rights to use or distribute this software, please contact Berkeley Lab's Innovation & Partnerships Office at  IPO@lbl.gov.
 *
 * NOTICE. This Software was developed under funding from the U.S. Department of Energy and the U.S. Government consequently retains certain rights. As such, the U.S. Government has been granted for itself and others acting on its behalf a paid-up, nonexclusive, irrevocable, worldwide license in the Software to reproduce, distribute copies to the public, prepare derivative works, and perform publicly and display publicly, and to permit other to do so. 
 *
 */

/**
 *
 * Email questions to {dbin, kwu, sbyna}@lbl.gov
 * Scientific Data Management Research Group
 * Lawrence Berkeley National Laboratory
 *
 */


#ifndef ARRAY_UDF_UTILITY
#define ARRAY_UDF_UTILITY

#include <vector>

using namespace std;

double time_address_cal = 0, row_major_order_cal = 0, pre_row_major_order_cal=0, data_access_time = 0, trail_run_time = 0;
unsigned long long RowMajorOrder(std::vector<unsigned long long> dsize, std::vector<unsigned long long> coordinate){
  unsigned long long offset = coordinate[0];
  int n = dsize.size();
  for (int i = 1; i < n; i++){
    offset = offset*dsize[i] + coordinate[i];
  }
  return offset;
}

std::vector<unsigned long long> RowMajorOrderReverse(unsigned long long offset, std::vector<unsigned long long> dsize){
  int n = dsize.size();
  std::vector<unsigned long long> original_coordinate;
  original_coordinate.resize(n);
  //unsigned long long reminder; 
  for (unsigned long long i = n-1; i >= 1; i--){
    //reminder = 
    original_coordinate[i] = offset % dsize[i];
    offset   = offset / dsize[i]; 
  }
  //Last dimenstion 
  original_coordinate[0] = offset;

#ifdef DEBUG
  std::cout << "offset = " << offset << std::endl;
  std::cout << "dsize [] = " << dsize[0] << ", " << dsize[1]<< std::endl;
  std::cout << "original_coordinate [] = " << original_coordinate[0] << ", " << original_coordinate[1]<< std::endl;
#endif
  
  return original_coordinate;
}


#endif
